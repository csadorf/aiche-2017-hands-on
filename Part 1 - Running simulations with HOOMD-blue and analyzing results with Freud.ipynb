{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# AIChE 2017 -- *Hands On With Molecular Simulation*\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 1 - Running simulations with HOOMD-blue and analyzing results with Freud\n",
    "\n",
    "This example shows how to initialize a system of particles and execute dissipative particle dynamics simulations with **HOOMD**. Then it demonstrates how to analyze the simulation output with **Freud**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initialization\n",
    "\n",
    "We start by initializing the execution context for HOOMD, which determines how the simulation is actually executed.\n",
    "When no command line options are provided, HOOMD will auto-select a GPU if one is available, or alternatively run on the CPU."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import hoomd\n",
    "import hoomd.md\n",
    "\n",
    "hoomd.context.initialize('');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we specify the initial conditions of the particles in the simulation. Define a square lattice of A and B particles:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "a = 0.6\n",
    "unitcell = hoomd.lattice.unitcell(N=4,\n",
    "                                  a1=(2*a, 0,   0),\n",
    "                                  a2=(0,   2*a, 0),\n",
    "                                  a3=(0,   0,   1),\n",
    "                                  dimensions=2,\n",
    "                                  position=[(0,0,0), (0,a,0), (a,0,0), (a,a,0)],\n",
    "                                  type_name=['A', 'B', 'B', 'A'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Replicate the unit cell to place many particles."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "snapshot = unitcell.get_snapshot()\n",
    "snapshot.replicate(15, 15, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "By default, particles have 0 velocity. Set a Gaussian random velocity using numpy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "snapshot.particles.velocity[:,0:2] = np.random.normal(0.0, np.sqrt(0.8 / 1.0), [snapshot.particles.N, 2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The lattice initialization placed an equal number of A and B particles. Randomly set 1/4 of the particles in the box to type B and the rest to type A."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "index_array = np.arange(snapshot.particles.N)\n",
    "np.random.shuffle(index_array)\n",
    "snapshot.particles.typeid[:] = snapshot.particles.types.index('A')\n",
    "snapshot.particles.typeid[index_array[:int(snapshot.particles.N/4)]] = snapshot.particles.types.index('B')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Initialize the system with this snapshot as the system configuration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "hoomd.init.read_snapshot(snapshot);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at the system initial configuration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from matplotlib import pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(4, 4), dpi=140)\n",
    "pos_a = snapshot.particles.position[snapshot.particles.typeid==snapshot.particles.types.index('A'), :]\n",
    "pos_b = snapshot.particles.position[snapshot.particles.typeid==snapshot.particles.types.index('B'), :]\n",
    "ax.plot(pos_a[:,0], pos_a[:,1], 'o')\n",
    "ax.plot(pos_b[:,0], pos_b[:,1], 'o')\n",
    "ax.get_xaxis().set_visible(False)\n",
    "ax.get_yaxis().set_visible(False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Force field definition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "In the DPD polymer model, pairs of particles closer than $r_\\mathrm{cut}$ interact with a linear conservative force, a drag force that depends on the relative velocity, and a random force. The parameter A specifies the strength of the repulsive force.\n",
    "\n",
    "Choose the neighbor list acceleration structure to find neighboring particles efficiently. In systems with only one cutoff length, the cell method performs best."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "nl = hoomd.md.nlist.cell()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Define the functional form of the pair interaction and evaluate using the given neighbor list acceleration structure. $kT$ defines the temperature of the system in energy units and *seed* defines the seed for the random number generator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "dpd = hoomd.md.pair.dpd(r_cut=1.0, nlist=nl, kT=0.8, seed=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Specify pair potential parameters for every pair of types in the simulation. Each line sets the strength of the repulsive force for A-A, A-B, and B-B particle interactions, respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "dpd.pair_coeff.set('A', 'A', A=10.0, gamma = 1.0)\n",
    "dpd.pair_coeff.set('A', 'B', A=10.0, gamma = 1.0)\n",
    "dpd.pair_coeff.set('B', 'B', A=10.0, gamma = 1.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Setting up the integrator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "The integrator defines the equations of motion that govern the system of particles, given the current configuration of the particles and the net force from all potentials. The standard integration mode in HOOMD allows different integrators to apply to different groups of particles with the same step size $dt$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "hoomd.md.integrate.mode_standard(dt=0.01);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "``pair.dpd`` applies the random and drag forces consistent with a thermal system and should be used with the *NVE* integrator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "all = hoomd.group.all();\n",
    "hoomd.md.integrate.nve(group=all);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Write Output"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Periodically log the potential energy of the system to a text file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "hoomd.analyze.log(filename='log-output.log',\n",
    "                  quantities=['potential_energy', 'temperature'],\n",
    "                  period=100,\n",
    "                  overwrite=True);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "hoomd.dump.gsd('trajectory.gsd', period=500, group=all, overwrite=True);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Run the simulation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Take 5000 steps forward in time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "hoomd.run(5000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Examining the output\n",
    "\n",
    "Use matplotlib to plot the potential energy and temperature vs time step.\n",
    "\n",
    "Good equilibration *may* be indicated by an extended period where the potential energy has leveled off."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "data = np.genfromtxt(fname='log-output.log', names=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(4, 2.2), dpi=140)\n",
    "ax.plot(data['timestep'][3:], data['potential_energy'][3:])\n",
    "ax.set_xlabel('time step')\n",
    "ax.set_ylabel('potential energy')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(4, 2.2), dpi=140)\n",
    "ax.plot(data['timestep'][3:], data['temperature'][3:])\n",
    "ax.set_xlabel('time step')\n",
    "ax.set_ylabel('temperature')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Structural analysis provides a much stronger signal for proper equilibration. \n",
    "\n",
    "First, examine the system configuration in the final frame of the trajectory. Use [gsd] to read the file and matplotlib to plot it.\n",
    "\n",
    "[gsd]: https://gsd.readthedocs.io"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import gsd\n",
    "import gsd.hoomd\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(4, 4), dpi=140)\n",
    "with gsd.hoomd.open('trajectory.gsd') as traj:\n",
    "    frame = traj[-1];\n",
    "    pos_a = frame.particles.position[frame.particles.typeid==frame.particles.types.index('A'), :]\n",
    "    pos_b = frame.particles.position[frame.particles.typeid==frame.particles.types.index('B'), :]\n",
    "    ax.plot(pos_a[:,0], pos_a[:,1], 'o')\n",
    "    ax.plot(pos_b[:,0], pos_b[:,1], 'o')\n",
    "    ax.get_xaxis().set_visible(False)\n",
    "    ax.get_yaxis().set_visible(False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use [freud] to cluster the B particles in every frame of the trajectory. Then, plot the number of clusters vs time.\n",
    "\n",
    "[freud]: https://glotzerlab.engin.umich.edu/freud"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import freud\n",
    "step = []\n",
    "n_clusters = []\n",
    "\n",
    "with gsd.hoomd.open('trajectory.gsd') as traj:\n",
    "    for frame in traj[1:]:\n",
    "        box = freud.box.Box(Lx=frame.configuration.box[0], Ly=frame.configuration.box[1], is2D=True)\n",
    "        cluster = freud.cluster.Cluster(box, 1.0)\n",
    "        pos_b = frame.particles.position[frame.particles.typeid==frame.particles.types.index('B'), :]\n",
    "        cluster.computeClusters(pos_b)\n",
    "        \n",
    "        step.append(frame.configuration.step)\n",
    "        n_clusters.append(cluster.getNumClusters())\n",
    "    \n",
    "fig, ax = plt.subplots(figsize=(4, 2.2), dpi=140)\n",
    "ax.plot(step, n_clusters, '-')\n",
    "ax.set_ylim(ymin=0)\n",
    "ax.set_xlabel('time step')\n",
    "ax.set_ylabel('number of B clusters')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print(n_clusters)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "\n",
    "### 1. Phase separation\n",
    "\n",
    "Modify one simulation parameter so that the A and B particles phase separate. After changing the value, select Cell->Run All to rerun the notebook and observe the result.\n",
    "\n",
    "### 2. Equilibration\n",
    "\n",
    "Now that you have the system phase separating, examine the simulation output carefully. Is the system fully equilibrated? Change the script and run it again and see if you can get the system to equilibrate completely.\n",
    "\n",
    "**In [part 2](Part 2 - Managing the Data Space with signac.ipynb) we convert this workflow into one using [signac](https://www.signac.io) for data management.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
