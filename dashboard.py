#!/usr/bin/env python
from signac_dashboard import Dashboard
from signac_dashboard.modules import ImageViewer
from signac_dashboard.modules import StatepointList


class MyDashboard(Dashboard):

    def job_title(self, job):
        return "A:{job.sp.mixture.A:0.2f} B:{job.sp.mixture.B:0.2f}".format(job=job)


if __name__ == '__main__':
    MyDashboard(
        modules=[
            StatepointList(),
            ImageViewer(),
            ],
        ).run()
