{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# AIChE 2017 -- *Hands On With Molecular Simulation*\n",
    "\n",
    "## Part 2 - Managing the Data Space with signac\n",
    "\n",
    "Part 1 demonstrates the setup and execution of a **single simulation**.\n",
    "In a more realistic scenario we might want to capture *input* and *output* metadata as well perform a computational investigation over multiple **variables of interest**.\n",
    "\n",
    "We will now demonstrate how to convert the workflow developed before into one where file and metadata management is handled with [signac].\n",
    "The **signac framework** is designed to assist large-scale multidimensional computational data generation and analysis. It is assumed that the work can be divided into *projects*, where each project is vaguely confined by roughly similar structured data, *e.g.*, a parameter study.\n",
    "\n",
    "A *signac project* is the interface to the data space that we will curate for this investigation.\n",
    "We start by importing the **signac** package and initializing the project.\n",
    "\n",
    "[Hoomd-Blue]: https://glotzerlab.engin.umich.edu/hoomd-blue\n",
    "[signac]: http://www.signac.io\n",
    "[freud]: https://glotzerlab.engin.umich.edu/freud"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data Space Initialization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import signac\n",
    "\n",
    "project = signac.init_project('AIChE-2017-Hands-On-Example-Project')\n",
    "print(project)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The command above created a configuration file within the current directory, which allows us to get access to the project from any script that is executed within the same directory with `project = signac.get_project()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "! cat signac.rc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*(Uncomment and run the cell below to remove all data from previous runs and start over.)*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# for job in project:\n",
    "#     job.remove()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's import some of the other packages required for the execution of this notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import hoomd\n",
    "import hoomd.md\n",
    "import gsd\n",
    "import gsd.hoomd\n",
    "from matplotlib import pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "hoomd.context.initialize('--notice-level=0')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we define the parameter space that we want to investigate.\n",
    "We start by identifying the *variable(s) of interest*: any input variable that we want to study the response of.\n",
    "\n",
    "In this particular case we are going to **vary only the mixing ratio**, but we will also capture some of the most important simulation protocol parameters, such as the temperature, and the *force field* specification.\n",
    "\n",
    "To initialize the *data space*, we simply iterate over the discretized parameter space, define a full *state point* and call the initialization function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for concentration_A in (0.25, 0.5, 0.75):\n",
    "    statepoint = {\n",
    "        # binary mixture component concentrations\n",
    "        'mixture': {'A': concentration_A, 'B': 1 - concentration_A},\n",
    "\n",
    "        # temperature\n",
    "        'kT': 0.8,\n",
    "\n",
    "        # force field parameters\n",
    "        'dpd': [\n",
    "            {'a': 'A', 'b': 'A', 'A': 10.0, 'gamma': 1.0},\n",
    "            {'a': 'A', 'b': 'B', 'A': 50.0, 'gamma': 1.0},\n",
    "            {'a': 'B', 'b': 'B', 'A': 10.0, 'gamma': 1.0},\n",
    "        ]\n",
    "    }\n",
    "    \n",
    "    # Initialize job for state point\n",
    "    job = project.open_job(statepoint)\n",
    "    job.init()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `job` instance is a handle for a specific *set of parameters* called a *state point*.\n",
    "\n",
    "We can easily iterate through the complete data space and inspect all of its constituent state points:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from pprint import pprint\n",
    "\n",
    "for job in project:\n",
    "    pprint(job.statepoint())\n",
    "    print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The *job handle* helps us associate this specific set of parameters and any kind of data by providing us with a specific **job id**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for job in project:\n",
    "    print(job.sp.mixture.A, job.get_id())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This *job id* is a unique function of the state point and can be used to associate any kind of data that is a function of the state point, *e.g.*, by using it as part of the file path:\n",
    "Instead of `c_A_75/init.gsd`, we can use: `5c82b72072da101dff2581c7dbc62c27/init.gsd`.\n",
    "While this might seem slightly more verbose at first glance, keep in mind that this id does not only encode the *concentration of A*, but also all other parameters which are part of the state point.\n",
    "\n",
    "File paths like the one above are the *primary storage pattern* with **signac**, which is why the job handle provides functions to easily wrap filenames to produce these kinds of paths:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for job in project:\n",
    "    print(job.sp.mixture.A, job.fn('init.gsd'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All operations that modify data associated with that particular set of parameters should be a function of that job handle, files should be stored in the given file paths.\n",
    "\n",
    "To simplify this process, we are going to combine all steps for the *setup* of the initial configuration into a separate function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def setup(job):\n",
    "    np.random.seed(0)\n",
    "    with hoomd.context.SimulationContext():\n",
    "        a = 0.6\n",
    "        unitcell = hoomd.lattice.unitcell(N=4,\n",
    "                                          a1=(2*a, 0,   0),\n",
    "                                          a2=(0,   2*a, 0),\n",
    "                                          a3=(0,   0,   1),\n",
    "                                          dimensions=2,\n",
    "                                          position=[(0,0,0), (0,a,0), (a,0,0), (a,a,0)],\n",
    "                                          type_name=['A', 'B', 'B', 'A'])\n",
    "\n",
    "        snapshot = unitcell.get_snapshot()\n",
    "        snapshot.replicate(15, 15, 1)\n",
    "        \n",
    "        N_A = int(snapshot.particles.N * job.sp.mixture.A)\n",
    "        index_array = np.arange(snapshot.particles.N)\n",
    "        np.random.shuffle(index_array)\n",
    "        snapshot.particles.typeid[:] = snapshot.particles.types.index('B')\n",
    "        snapshot.particles.typeid[index_array[:N_A]] = snapshot.particles.types.index('A')\n",
    "\n",
    "        hoomd.init.read_snapshot(snapshot)\n",
    "        snapshot.particles.velocity[:,0:2] = np.random.normal(0.0, np.sqrt(0.8 / 1.0), [snapshot.particles.N, 2])\n",
    "        hoomd.dump.gsd(job.fn('init.gsd'), period=None, group=hoomd.group.all())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then simply iterate over the complete data space and call that function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for job in project:\n",
    "    setup(job)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An even better approach is to make the function execution conditional on the existence of the `init.gsd` file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for job in project:\n",
    "    if not job.isfile('init.gsd'):\n",
    "        setup(job)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You will notice that the function was not executed this time, because all jobs have already been initialized.\n",
    "\n",
    "We will also integrate all functions used for carrying out the simulation into a function, by copy & pasting from Part 1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def simulate(job):\n",
    "    with hoomd.context.SimulationContext():\n",
    "        system = hoomd.init.read_gsd('init.gsd', restart='restart.gsd')\n",
    "        nl = hoomd.md.nlist.cell()\n",
    "        dpd = hoomd.md.pair.dpd(r_cut=1.0, nlist=nl, kT=job.sp.kT, seed=0)\n",
    "        for ff in job.sp.dpd:\n",
    "            dpd.pair_coeff.set(**ff)  # We used force field parameter names that match the function parameter names.\n",
    "        hoomd.md.integrate.mode_standard(dt=0.01)\n",
    "        hoomd.md.integrate.nve(group=hoomd.group.all())\n",
    "        hoomd.analyze.log(filename='log-output.log',\n",
    "              quantities=['potential_energy', 'temperature'],\n",
    "              period=500)\n",
    "        hoomd.dump.gsd('trajectory.gsd', period=500, group=hoomd.group.all());\n",
    "        hoomd.run_upto(5000)\n",
    "        hoomd.dump.gsd('final.gsd', period=None, group=hoomd.group.all())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, all previously hard-coded parameters have been replaced with the respective values that are part of the state point.\n",
    "\n",
    "You might have noticed that we have not wrapped any of the filenames this time.\n",
    "That is because we will use a slightly different approach to ensure that all data is read from and stored at the correct location: we just change into the correct *workspace* directory before execution.\n",
    "\n",
    "We execute the simulation operation contingent of the existence and non-existence of the corresponding input and output files:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for job in project:\n",
    "    if job.isfile('init.gsd') and not job.isfile('final.gsd'):\n",
    "        with job:  # switch into the job's workspace\n",
    "            simulate(job)\n",
    "print('Done.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Aside: Execution with signac-flow\n",
    "\n",
    "While it is in principle possible to execute even computationally demanding operations within a jupyter notebook, we recommend to use separate scripts for functions such as the one above.\n",
    "\n",
    "In fact, we can utilize the [signac-flow](https://signac-flow.readthedocs.io) package for the execution of high throughput operations locally and for scheduling on high-performance clusters.\n",
    "For the example above, we would simply copy & paste the function into a separate script (*e.g.* `operations.py`) and then augment it with the `flow.run()` interface:\n",
    "```\n",
    "# operations.py\n",
    "\n",
    "def simulate(job):\n",
    "    with job:\n",
    "        # [...]\n",
    "    \n",
    "if __name__ == '__main__':\n",
    "    import flow\n",
    "    flow.run()\n",
    "```\n",
    "This would allow us to execute the *simulation* operation directly from the command line with: `$ python operations.py simulate`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Analysis\n",
    "\n",
    "Analysis routines are implemented just as in [part one](Part 1 - Running simulations with HOOMD-blue and analyzing results with Freud.ipynb), however here we can take advantage of **signac**'s search capabilities to examine a specific state point of interest:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We first gather the possible concentrations of *A* and then select the associated jobs using signac's `project.find_jobs()` selection function, displaying plots for each of them.\n",
    "\n",
    "*The \"`for job in project:`\" syntax is just short-hand for \"`for job in project.find_jobs()`\".*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "concentrations_A = list(sorted(set([job.sp.mixture.A for job in project])))\n",
    "\n",
    "for conc_A in concentrations_A:\n",
    "    jobs = project.find_jobs({'mixture.A': conc_A})\n",
    "    \n",
    "    data = [np.genfromtxt(job.fn('log-output.log'), names=True) for job in jobs if job.isfile('log-output.log')]\n",
    "    time = data[0]['timestep']\n",
    "    fig, axes = plt.subplots(figsize=(8, 2.2), dpi=140, ncols=2)\n",
    "    for field, ax in zip(['temperature', 'potential_energy'], axes):\n",
    "        y = [d[field] for d in data]\n",
    "        ax.errorbar(time, np.mean(y, axis=0), np.std(y, axis=0))\n",
    "        ax.set_xlabel('Time step')\n",
    "        ax.set_ylabel(field)\n",
    "        ax.set_title('$C_A={}$'.format(conc_A))\n",
    "    fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can select jobs for the rendering and display the final frame in the same way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for conc_A in concentrations_A:\n",
    "    job = list(project.find_jobs({'mixture.A': conc_A}))[0]\n",
    "    \n",
    "    fig, ax = plt.subplots(figsize=(4, 4), dpi=140)\n",
    "    with gsd.hoomd.open(job.fn('trajectory.gsd')) as traj:\n",
    "        frame = traj[-1];\n",
    "        pos_a = frame.particles.position[frame.particles.typeid==frame.particles.types.index('A'), :]\n",
    "        pos_b = frame.particles.position[frame.particles.typeid==frame.particles.types.index('B'), :]\n",
    "        ax.plot(pos_a[:,0], pos_a[:,1], 'o')\n",
    "        ax.plot(pos_b[:,0], pos_b[:,1], 'o')\n",
    "        ax.get_xaxis().set_visible(False)\n",
    "        ax.get_yaxis().set_visible(False)\n",
    "        ax.set_title('$C_A={}$'.format(conc_A))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Job Document\n",
    "\n",
    "The [job document](http://signac.readthedocs.io/en/latest/projects.html#the-job-document) allows us to store searchable data and metadata with our job, that is more *dynamic* than the metadata stored as part of the state point.\n",
    "\n",
    "For example, we might want to select our jobs by the number of clusters in the final frame.\n",
    "For this, we first implement a routine to compute *the number of clusters over time*, just as in part 1:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import freud\n",
    "\n",
    "def compute_clusters(job, species):\n",
    "    step = []\n",
    "    n_clusters = []\n",
    "\n",
    "    with gsd.hoomd.open(job.fn('trajectory.gsd')) as traj:\n",
    "        for frame in traj[1:]:\n",
    "            box = freud.box.Box(Lx=frame.configuration.box[0], Ly=frame.configuration.box[1], is2D=True)\n",
    "            cluster = freud.cluster.Cluster(box, 1.0)\n",
    "            pos_b = frame.particles.position[frame.particles.typeid==frame.particles.types.index(species), :]\n",
    "            cluster.computeClusters(pos_b)\n",
    "\n",
    "            step.append(frame.configuration.step)\n",
    "            n_clusters.append(cluster.getNumClusters())\n",
    "    return n_clusters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we iterate over all jobs, and store the final number of clusters in the job document:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for job in project:\n",
    "    if job.isfile('trajectory.gsd'):\n",
    "        clusters = {species: compute_clusters(job, species)[-1] for species in ('A', 'B')}\n",
    "        job.document['n_clusters_final'] = clusters\n",
    "        print(job, clusters)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just like we can use a *filter* to select jobs by state point values, we can use a *doc_filter* to select jobs by document values.\n",
    "\n",
    "Here we show all jobs, where the number of *B* clusters in the final frame is greater than one:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for job in project.find_jobs(doc_filter={'n_clusters_final.B': {'$gt': 1}}):\n",
    "    print('mix', job.sp.mixture, 'n_clusters_final', job.document['n_clusters_final'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data space modification\n",
    "\n",
    "A common requirement is the need to **modify the data space** after the fact.\n",
    "For example, we might be satisfied with the preliminary results generated above, but might want to add some replication to determine the statistical uncertainty.\n",
    "\n",
    "For this we are going to vary the *random seed* and make it an explicit part of our state point.\n",
    "First, we need to **migrate** the existing jobs to add the random seed explicitly to the state point:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Migrate existing jobs and remove duplicates\n",
    "for job in project:\n",
    "    if 'seed' not in job.sp:\n",
    "        try:\n",
    "            job.sp.seed = 0\n",
    "        except signac.errors.DestinationExistsError:\n",
    "            job.remove()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We could modify and execute the initialization loop from above again, but instead we are going to copy the existing job statepoints and initialize copies with different random seeds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for job in project:\n",
    "    sp = job.statepoint()\n",
    "    for seed in 0, 1:\n",
    "        sp['seed'] = seed\n",
    "        project.open_job(sp).init()\n",
    "        \n",
    "for job in project:\n",
    "    print(job, job.sp.mixture.A, job.sp.seed)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we update our operations from above to replace the previously hard-coded random seed with the one from the state point:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def setup(job):\n",
    "    # ################################### #\n",
    "    #                                     #\n",
    "    # REPLACED HARD-CODED RANDOM SEED HERE:\n",
    "    np.random.seed(job.sp.seed)\n",
    "    \n",
    "    with hoomd.context.SimulationContext():\n",
    "        a = 0.6\n",
    "        unitcell = hoomd.lattice.unitcell(N=4,\n",
    "                                          a1=(2*a, 0,   0),\n",
    "                                          a2=(0,   2*a, 0),\n",
    "                                          a3=(0,   0,   1),\n",
    "                                          dimensions=2,\n",
    "                                          position=[(0,0,0), (0,a,0), (a,0,0), (a,a,0)],\n",
    "                                          type_name=['A', 'B', 'B', 'A'])\n",
    "\n",
    "        snapshot = unitcell.get_snapshot()\n",
    "        snapshot.replicate(15, 15, 1)\n",
    "        \n",
    "        N_A = int(snapshot.particles.N * job.sp.mixture.A)\n",
    "        index_array = np.arange(snapshot.particles.N)\n",
    "        np.random.shuffle(index_array)\n",
    "        snapshot.particles.typeid[:] = snapshot.particles.types.index('B')\n",
    "        snapshot.particles.typeid[index_array[:N_A]] = snapshot.particles.types.index('A')\n",
    "\n",
    "        hoomd.init.read_snapshot(snapshot)\n",
    "        snapshot.particles.velocity[:,0:2] = np.random.normal(0.0, np.sqrt(0.8 / 1.0), [snapshot.particles.N, 2])\n",
    "        hoomd.dump.gsd(job.fn('init.gsd'), period=None, group=hoomd.group.all())\n",
    "        \n",
    "def simulate(job):\n",
    "    with hoomd.context.SimulationContext():\n",
    "        system = hoomd.init.read_gsd('init.gsd', restart='restart.gsd')\n",
    "        nl = hoomd.md.nlist.cell()\n",
    "        # ################################### #\n",
    "        #                                     #\n",
    "        # REPLACED HARD-CODED RANDOM SEED HERE:\n",
    "        dpd = hoomd.md.pair.dpd(r_cut=1.0, nlist=nl, kT=job.sp.kT, seed=job.sp.seed)\n",
    "        for ff in job.sp.dpd:\n",
    "            dpd.pair_coeff.set(**ff)  # We used force field parameter names that match the function parameter names.\n",
    "        hoomd.md.integrate.mode_standard(dt=0.01)\n",
    "        hoomd.md.integrate.nve(group=hoomd.group.all())\n",
    "        hoomd.analyze.log(filename='log-output.log',\n",
    "              quantities=['potential_energy', 'temperature'],\n",
    "              period=500)\n",
    "        hoomd.dump.gsd('trajectory.gsd', period=500, group=hoomd.group.all());\n",
    "        hoomd.run_upto(5000)\n",
    "        hoomd.dump.gsd('final.gsd', period=None, group=hoomd.group.all())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's execute the complete workflow within one cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "for job in project:\n",
    "    print(job)\n",
    "    if not job.isfile('init.gsd'):\n",
    "        setup(job)\n",
    "    if job.isfile('init.gsd') and not job.isfile('final.gsd'):\n",
    "        with job:  # switch into the job's workspace\n",
    "            simulate(job)\n",
    "print('Done.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now run the analysis section again, to visualize the statistical certainty!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "\n",
    "### 1. Store rendered snapshot images\n",
    "\n",
    "Store an image of the last snapshot in a file called `snapshot.png` in each job's workspace.\n",
    "\n",
    "Hint: Use matplotlib's [`savefig()`] function.\n",
    "\n",
    "### 2. Cached cluster analysis\n",
    "\n",
    "Implement a function `get_clusters(job, species)` that returns the number of clusters for all time steps by loading them from the job document or calculating them in case that they have not been stored yet ([memoization]).\n",
    "\n",
    "### 3. Highest cluster density for A/B\n",
    "\n",
    "Find the two concentrations that result in either the highest number of A cluster or the highest number of B clusters.\n",
    "\n",
    "### Bonus: Remove code duplication\n",
    "\n",
    "Modify the notebook to account for the seed variable without code duplication:\n",
    "\n",
    "1. Add the random seed directly to the initialization loop:\n",
    "```\n",
    "for concentration_A in (0.25, 0.5, 0.75):\n",
    "        for seed in (0, 1):\n",
    "                statepoint = {\n",
    "                     'seed': seed,\n",
    "                # ...\n",
    "                }   \n",
    "# Initialize job for state point\n",
    "job = project.open_job(statepoint).init()\n",
    "```\n",
    "2. Replace the original `setup()` routine to use the correct random seed and remove the duplicate.\n",
    "3. Replace the original `simulate()` routine to use the correct random seed and remove the duplicate.\n",
    "4. Combine the different *operation execution cells* into a single cell.\n",
    "\n",
    "*A robust notebook implementation should essentially be [reentrant].*\n",
    "\n",
    "### Solutions\n",
    "\n",
    " * [Link to notebook with proposed solutions.](Part 2 - With Proposed Solutions.ipynb)\n",
    " * [Link to notebook with proposed solutions only.](Part 2 - Proposed Solutions Only.ipynb)\n",
    "\n",
    "[`savefig()`]: http://matplotlib.org/api/_as_gen/matplotlib.pyplot.savefig.html\n",
    "[memoization]: https://en.wikipedia.org/wiki/Memoization\n",
    "[reentrant]: https://en.wikipedia.org/wiki/Reentrancy_(computing)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
